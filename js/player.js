Player = function(){
    this.orientation = Player.orientation.Right; 
    this.baseSpeed= 2;
    this.speed = 2;
    this.sprint = 5;
    this.baseJump = 0;
    this.currentJump = 0;
    this.maxJump = 8;
    this.isJumping = false;
    this.maxHealth = 150;
    this.currentHealth = 150;
    this.healthBar = new PIXI.Graphics();
    this.sprite = null;
    this.state = 
      { 
        "idle" : new State("idle", 0), 
        "jumping" : new State("jumping", 15),
        "walking" : new State("walking", 0),
        "autoAttacking" : new State("autoAttacking", 0),
        "skill1" : new State("skill1", 25),
        "skill2" : new State("skill2", 15),
        "skill3" : new State("skill3", 25),
        "attacking" : new State("attacking", 0)
      };
    this.sequences = null;
    this.loadFrames();
    this.sprite.debugGraphic = new PIXI.Graphics();
    this.sprite.frameRate = 2;
    this.sprite.position.x = 300;
    this.sprite.position.y = 300;
    this.sprite.onComplete = this.completedSequences;
    this.healthBar.beginFill(0xFF0000);
    this.healthBar.lineStyle(1, 0xFFFFFF);
    this.healthBar
      .drawRect(this.sprite.position.x, 
                this.sprite.position.y + 15,
                (this.currentHealth / this.maxHealth) * 100,
                5);
    this.sprite.hitBox = new PIXI.Rectangle(this.sprite.position.x - (this.sprite.width / 2), this.sprite.position.y - (this.sprite.height / 2) , this.sprite.width, this.sprite.height);  
    this.sprite.play();
};

Player.prototype.loadFrames = function(){
  
    var walkFrames = [];

    for (var i = 0; i < 4; i++) {
      walkFrames.push(PIXI.Texture.fromFrame('warriorWalk' + i + ".png"));
    }
    
    var jumpFrames = [];
    
    for (var x =0; x < 1; x++) {
      jumpFrames.push(PIXI.Texture.fromFrame("warriorJump" + x + ".png"));
    }
    
    var standFrames = [];
    
    for (var y = 0; y < 3; y++) {
      standFrames.push(PIXI.Texture.fromFrame("warriorStand" + y + ".png"));
    }
    
    var swingO1Frames = [];
    
    for (var z = 0; z < 3; z++){
      swingO1Frames.push(PIXI.Texture.fromFrame("warriorSwingO1_" + z + ".png"));
    }
    
    var swingO2Frames = [];
    
    for (var a = 0; a < 3; a++){
      swingO2Frames.push(PIXI.Texture.fromFrame("warriorSwingO2_" + a + ".png"));
    }
    
    var swingO3Frames = [];
    
    for (var b = 0; b <3; b++){
      swingO3Frames.push(PIXI.Texture.fromFrame("warriorSwingO3_" + b + ".png"));
    }
    
    var swingOFFrames = [];
    
    for (var c = 0; c < 4; c++) {
      swingOFFrames.push(PIXI.Texture.fromFrame("warriorSwingOF_" + c + ".png"));
    }
    this.sequences = {
      "idle": standFrames,
      "walk" : walkFrames,
      "jump" : jumpFrames,
      "autoAttack" : swingO1Frames,
      "skill1"  : swingO2Frames,
      "skill2" : swingO3Frames,
      "skill3" : swingOFFrames
    };
  
    this.sprite = new PIXI.AnimatedSprite(this.sequences);
};

Player.orientation = {
  Left : -1,
  Right : 1
  
};

Player.prototype.isStandingOnObject = function(){
  return Physics.isPointOnBottomOfLevel(new PIXI.Point(this.sprite.hitBox.x, this.sprite.hitBox.y + this.sprite.hitBox.height + 9));
}
Player.prototype.completedSequences = function(sprite, completed)
{
  // oddly enough this in this context is referencing the sprite, and not the player.
  // I have no idea why, but i'm just going to roll with it.
  // console.log(this);
  this.isAttacking = false;
  if (completed == "autoAttack") {
    sprite.gotoAndPlay("idle");
    this.debugGraphic.clear();
  }
  if (completed == "skill1") {
    sprite.gotoAndPlay("idle");
    this.debugGraphic.clear();
  }
  
  if (completed == "skill2") {
    sprite.gotoAndPlay("idle");
    this.debugGraphic.clear();
  }
  
  if (completed == "skill3") {
    sprite.gotoAndPlay("idle");
    this.debugGraphic.clear();
  }
};