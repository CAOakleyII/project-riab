InputManager = function(stage, level){
  this.stage = stage;
  this.level = level;
};

InputManager.inputType = {
  Up : 0,
  Right : 1,
  Down : 2,
  Left : 3,
  Jump: 4,
  Sprint : 5,
  AutoAttack : 6,
  Skill1 : 7,
  Skill2 : 8,
  Skill3 : 9
};

InputManager.getInputType = function(key)
{
  switch(key)
  {
    case 39:
      return InputManager.inputType.Right;
    case 37:
      return InputManager.inputType.Left;
    case 38:
      return InputManager.inputType.Up;
    case 40:
      return InputManager.inputType.Down;
    case 32:
      return InputManager.inputType.Jump;
    case 16:
      return InputManager.inputType.Sprint;
    case 70:
      return InputManager.inputType.AutoAttack;
    case 65:
      return InputManager.inputType.Skill1;
    case 83:
      return InputManager.inputType.Skill2;
    case 68:
      return InputManager.inputType.Skill3;
  }
};



InputManager.prototype.readInput = function(player){
      if (!player.sprite.isAttacking) {
      
      // Right Movement
      if (InputManager.inputType.Right in keysDown) {
        if(player.sprite.currentSequence != "walk" || !player.sprite.playing){
          player.sprite.frameRate = 7;
          player.sprite.gotoAndPlay("walk");
        }
        
        player.sprite.scale.x = -1;
        player.orientation = Player.orientation.Right;
        player.sprite.addImpulse(player.orientation * player.speed, 0);
        player.sprite.isWalking = true;
      }
      
      // Left Movement
      if (InputManager.inputType.Left in keysDown) {
        if(player.sprite.currentSequence != "walk" || !player.sprite.playing){
          player.sprite.frameRate = 7;
          player.sprite.gotoAndPlay("walk");
        }
          
        player.sprite.scale.x = 1;
        player.orientation = Player.orientation.Left;
        player.sprite.addImpulse(player.orientation * player.speed, 0);
        player.sprite.isWalking = true;
      }
      
      if (!(InputManager.inputType.Left in keysDown) && !(InputManager.inputType.Right in keysDown))
      {
        if(player.sprite.isWalking){
          player.sprite.isWalking = false;
          player.sprite.frameRate = 2;
          player.sprite.gotoAndPlay("idle");
        }
      }
      
      if (InputManager.inputType.Sprint in keysDown) {
        player.sprite.frameRate = 10;
        if (player.speed < player.sprint)
          player.speed += 0.2;
      } else { 
        if (player.speed > player.baseSpeed)
          player.speed  -= 0.2;
      }
    }

    // currently only allows the player to jump once they hit the floor.
    // the last conditional needs to be changed so they can jump once they are considered
    // "standing on an object"
    
    if (InputManager.inputType.Jump in keysDown && !player.sprite.isJumping & player.isStandingOnObject()) { 
      player.state.jumping.duration = 15;
      player.sprite.isJumping = true;
    }
    
    if(player.sprite.isJumping && player.state.jumping.duration > 0){
      player.sprite.gotoAndPlay("jump");
      player.sprite.addImpulse(0, -10);
      player.state.jumping.duration -= 1;
      
      if(player.state.jumping.duration <= 0)
      {
        player.sprite.isJumping = false;
      }
    }
  
    
    // Auto Attack
    if (InputManager.inputType.AutoAttack in keysDown && !player.sprite.isAttacking)
    {
      player.sprite.frameRate = 7;
      player.sprite.isAttacking = true;
      player.sprite.gotoAndPlay("autoAttack");
      
      var skillRect = new PIXI.Rectangle(player.sprite.position.x, player.sprite.position.y, player.orientation * 75, 50);
      
      player.sprite.debugGraphic.lineStyle(1, 0xFFFFFF);
      player.sprite.debugGraphic.drawRect(skillRect.x, skillRect.y, skillRect.width, skillRect.height);
      
      for (var x = 0; x < this.enemies.length; x++){
        var enemy = this.enemies[x];
        if(Physics.isColliding(skillRect, enemy.hitBox))
        {
          enemy.currentHealth -= 10;
        }
      }
    }
    
    
    // Skill 1
    if (InputManager.inputType.Skill1 in keysDown && !player.sprite.isAttacking){
      player.sprite.frameRate = 7;
      player.sprite.isAttacking = true;
      player.sprite.gotoAndPlay("skill1");
      
      var skillRect = new PIXI.Rectangle(player.sprite.position.x, player.sprite.position.y, player.orientation * 35, 80);
      
      player.sprite.debugGraphic.lineStyle(1, 0xFFFFFF);
      player.sprite.debugGraphic.drawRect(skillRect.x, skillRect.y, skillRect.width, skillRect.height);
      
      for (var x = 0; x < this.enemies.length; x++){
        var enemy = this.enemies[x];
        if(Physics.isColliding(skillRect, enemy.hitBox))
        {
          enemy.currentHealth -= 5;
        }
      }
    }
    
    // Skill 2
    if (InputManager.inputType.Skill2 in keysDown && !player.sprite.isAttacking){
      player.sprite.frameRate = 7;
      player.sprite.isAttacking = true;
      player.sprite.gotoAndPlay("skill2");
      
      var skillRect = new PIXI.Rectangle(player.sprite.position.x, player.sprite.position.y, player.orientation * 95, 30);
      
      player.sprite.debugGraphic.lineStyle(1, 0xFFFFFF);
      player.sprite.debugGraphic.drawRect(skillRect.x, skillRect.y, skillRect.width, skillRect.height);
      
      for (var x = 0; x < this.enemies.length; x++){
        var enemy = this.enemies[x];
        if(Physics.isColliding(skillRect, enemy.hitBox))
        {
          enemy.currentHealth -= 15;
        }
      }
    }
    
    // Skill 3
    if (InputManager.inputType.Skill3 in keysDown && !player.sprite.isAttacking){
      player.sprite.frameRate = 7;
      player.sprite.isAttacking = true;
      player.sprite.gotoAndPlay("skill3");
      
      var skillRect = new PIXI.Rectangle(player.sprite.position.x - (player.sprite.width / 2), player.sprite.position.y - (player.sprite.height / 2), player.orientation * 120, 70);
      
      player.sprite.debugGraphic.lineStyle(1, 0xFFFFFF);
      player.sprite.debugGraphic.drawRect(skillRect.x, skillRect.y, skillRect.width, skillRect.height);
      
      for (var x = 0; x < this.enemies.length; x++){
        var enemy = this.enemies[x];
        if(Physics.isColliding(skillRect, enemy.hitBox))
        {
          enemy.currentHealth -= 30;
        }
      }
    }
    
    player.sprite.advanceTime(1/60);
    player.healthBar.position.x = player.sprite.position.x - 350;
    player.healthBar.position.y = player.sprite.position.y - 375;
    
                                    
  // check if feet are touching level. (if so make character stand)
  // need to change once other world objects are in game to looop through 
  // objects and see if it's standing on anyhting
  if (!player.sprite.playing && !player.sprite.isWalking && player.isStandingOnObject()){
    
    
    if (player.state.idle.duration > 0){
      player.state.idle.duration -= 1;
    }
    else { 
      player.sprite.frameRate = 2;
      player.sprite.gotoAndPlay("idle");
      player.state.idle.duration = 15;
    }
  }
};


var keysDown = [];

$(document).keydown(function(e){
	
	// space and arrow keys
  // prevent them from scrolling the page
	if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
	  e.preventDefault();
  }
  
  var input = InputManager.getInputType(e.keyCode);
  keysDown[input] = true;
  
});

$(document).keyup(function(e) {
  var input = InputManager.getInputType(e.keyCode);
  delete keysDown[input];
});
