Wyvern = function(maxHealth) {
  this.takesPlayerDamage = true;
  this.maxHealth = maxHealth;
  this.currentHealth = maxHealth;
  this.healthBar = new PIXI.Graphics();
  this.isPhysicalObject = true;
  var sequences = loadFrames();
  PIXI.AnimatedSprite.call(this, sequences);
  
  this.hitBox = new PIXI.Rectangle(this.position.x - (this.width / 2), this.position.y - (this.height / 2) , this.width, this.height);
  
};


var loadFrames = function(){
  var idleFrames = [];
  
  for (var i = 0; i < 1; i++) {
    idleFrames.push(PIXI.Texture.fromFrame('wyvern.png'));
  }
  
  var sequences = {
    "idle" : idleFrames
  };
  
  return sequences;
};

Wyvern.constructor = Wyvern;
Wyvern.prototype = Object.create(PIXI.AnimatedSprite.prototype);

Wyvern.prototype.updateHitBox = function()
{
  this.hitBox.x = this.position.x - (this.width / 2);
  this.hitBox.y = this.position.y - (this.height / 2);
  
};


Wyvern.prototype.isOutsideFrame = function(point) {
  if (point.x >= 800 || point.x <= 0)
    return true;
  if (point.y >= 400 || point.y <= 0)
    return true;
};