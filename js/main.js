window.onload = function() {

  var renderer = PIXI.autoDetectRenderer(1024, 768);
  
  $('body').append(renderer.view);
  
  // create the root of the scene graph
  var stage = new PIXI.Container();
  var level = new PIXI.Rectangle(0,0,1024, 738);
  
  PIXI.loader
      .add('assets/basics/warrior_walk_1.json')
      .add('assets/basics/warrior_jump.json')
      .add('assets/basics/warrior_stand.json')
      .add('assets/basics/warrior_swing_O1.json')
      .add('assets/basics/warrior_swing_O2.json')
      .add('assets/basics/warrior_swing_O3.json')
      .add('assets/basics/warrior_swing_OF.json')
      .add('assets/basics/wyvern.json')
      .load(onAssetsLoaded);
  
  var Acceleration = 1.3;
  var player;
  var playersInfo;
  var frameBoxes;
  var enemies = [];
  var enemy;
  var inputManager = new InputManager(stage, level);
  var physics = new Physics(stage, level);
  
  function loadPlayer()
  {
    player = new Player();
  }


  function loadEnemies() {
    enemy = new Wyvern(1000);
    enemy.position.x = 700;
    enemy.position.y = 300;
    enemies.push(enemy);
    
    
    frameBoxes.lineStyle(1, 0xFFFFFF);
    
    frameBoxes.drawRect(enemy.hitBox.x, enemy.hitBox.y, enemy.hitBox.width, enemy.hitBox.height);
    
    stage.addChild(enemy);
  }
  
  function loadDebug(){
    var style = {
      font : '10px Arial',
      fill : '#FFFFFF',
      stroke : '#FFFFFF'  
    };
    
    playersInfo = new PIXI.Text("Width: " + player.sprite.width + "\n" +
                                    "Height: " + player.sprite.height + "\n" +
                                    "X: " + player.sprite.position.x + "\n" +
                                    "Y: " + player.sprite.position.y ,style);
    stage.addChild(player.sprite.debugGraphic);
    playersInfo.x = 5;
    playersInfo.y = 5;
    stage.addChild(playersInfo);
  }
  
  function onAssetsLoaded()
  {
      frameBoxes = new PIXI.Graphics();
      
      loadPlayer();
      loadDebug();
      loadEnemies();
      stage.addChild(frameBoxes);
      stage.addChild(player.sprite);
      stage.addChild(player.healthBar);
      inputManager.enemies = enemies;
      animate();
  }

  // *** GAME LOOP ***
  function animate() {
    
    inputManager.readInput(player);
    
    for (var i = 0; i < stage.children.length; i++)
    {
      physics.applyGravity(stage.children[i]);
    }
    
    playersInfo.text = "Width: " + player.sprite.width + "\n" +
                                    "Height: " + player.sprite.height + "\n" +
                                    "X: " + player.sprite.position.x + "\n" +
                                    "Y: " + player.sprite.position.y + "\n" +
                                    "enemy health :"+ enemy.currentHealth + "\n" +
                                    "enemy x :" + enemy.position.x + "\n" +
                                    "enemy y " + enemy.position.y + "\n" +
                                    "enemy width" + enemy.height;
                                    
    frameBoxes.clear();
    frameBoxes.lineStyle(1, 0xFFFFFF);
    
    enemy.updateHitBox();
    frameBoxes.drawRect(enemy.hitBox.x, enemy.hitBox.y, enemy.hitBox.width, enemy.hitBox.height);
    
    player.sprite.updateHitBox();
    frameBoxes.drawRect(player.sprite.hitBox.x, player.sprite.hitBox.y, player.sprite.hitBox.width, player.sprite.hitBox.height);
                
    // render the stage container
    renderer.render(stage);

    requestAnimationFrame(animate);
  }
};
