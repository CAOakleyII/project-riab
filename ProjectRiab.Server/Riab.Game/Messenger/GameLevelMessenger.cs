﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riab.Game.Messengers
{
    using Riab.Domain.Messages;
    using Riab.Domain.Services;
    
    /// <summary>
    /// The messager for the game level to communicate back to clients.
    /// </summary>
    public class GameLevelMessenger
    {
        /// <summary>
        /// The notifier used by this messager to communicate to clients.
        /// </summary>
        private readonly INotifier _notifier;

        /// <summary>
        /// The id of the room the messenger.
        /// </summary>
        private string roomId;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLevelMessenger"/>.
        /// </summary>
        /// <param name="roomId">The id of the room.</param>
        /// <param name="notifier">The notifier to communicate with clients.</param>
        public GameLevelMessenger(
            string roomId,
            INotifier notifier)
        {
            this.roomId = roomId;
            this._notifier = notifier;
        }

        /// <summary>
        /// This is just a mock implementation. Sends the game state to all players.
        /// </summary>
        public void SendGameState(dynamic gameData)
        {
            var message = new GameMessage()
                          {
                              RoomId = this.roomId,
                              MessageType = "GameState",
                              Data = gameData
                          };

            _notifier.SendGameMessage(message);
        }
    }
}