﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riab.Game
{
    using Riab.Domain.Services;
    using Riab.Game.Messengers;

    /// <summary>
    /// The game level is a representation of a current game instance, containing players, a game loop, and game logic. This is
    /// essentially the "game" part the handles the players input and controls the game.
    /// </summary>
    public class GameLevel
    {
        private readonly GameLevelMessenger _gameLevelMessenger;
        private const int numberOfSecondsToWaitBetweenUpdates = 1;
        private bool running = false;

        private long lastUpdateTicks;

        public GameLevel(
            string roomId,
            INotifier notifier)
        {
            _gameLevelMessenger = new GameLevelMessenger(roomId, notifier);
        }

        public void Start()
        {
            running = true;
            lastUpdateTicks = DateTime.Now.Ticks;
            while (running)
            {
                var now = DateTime.Now.Ticks;
                var differenceInMilliseconds = (now - lastUpdateTicks) / TimeSpan.TicksPerMillisecond;
                if (differenceInMilliseconds > numberOfSecondsToWaitBetweenUpdates * 1000)
                {
                    this.Update();
                    lastUpdateTicks = DateTime.Now.Ticks;
                }

            }
        }

        public void End()
        {
            running = false;
        }

        public void Update()
        {
            // calculate the delta
            // handle user input
            // update the game world
            // send updates to clients
            dynamic data = new { Time = DateTime.Now };
            _gameLevelMessenger.SendGameState(data);
        }

    }
}