﻿using Riab.Domain.Entities;
using Riab.Domain.Messages;
using Riab.Domain.Services;
using System.Collections.Generic;

namespace Riab.Game
{
    using System.Threading;

    public class PlayRoom
    {
        private readonly INotifier _notifier;

        private readonly Thread gameThread;

        /// <summary>
        /// Gets or sets the id of the play room.
        /// </summary>
        public string Id { get; private set; }

        public GameLevel GameLevel { get; private set; }

        /// <summary>
        /// Gets or sets the list of users
        /// </summary>
        public List<IUser> Users { get; private set; }

        public PlayRoom(string id, INotifier notifier)
        {
            this.Id = id;
            _notifier = notifier;
            Users = new List<IUser>();
            GameLevel = new GameLevel(Id, notifier);
            gameThread = new Thread(GameLevel.Start);
        }

        public void UserConnected(IUser user)
        {
            Users.Add(user);

            var message = new GameMessage{
                RoomId = Id,
                MessageType = "Message",
                Data = new {
                    Message = "Player joined. id: " + user.Id
                }
            };

            _notifier.SendGameMessage(message);
            _notifier.SendGameMessageToPlayerId(user.Id, message);

            if (!gameThread.IsAlive)
            {
                gameThread.Start();
            }
        }

        public void UserDisconnected(IUser user)
        {
            Users.Remove(user);

            var message = new GameMessage{
                RoomId = Id,
                MessageType = "Message",
                Data = new {
                    Message = "Player disconnected. id: " + user.Id
                }
            };

            _notifier.SendGameMessage(message);
            GameLevel.End();
        }

        public void UserMessage(ClientMessage message)
        {
            switch(message.MessageType){
                case "Message":
                     var outgoingMessage = new GameMessage{
                        RoomId = Id,
                        MessageType = "Message",
                        Data = new {
                            Message = string.Format("Recieved message from id: {0} Message: {1}", message.ConnectionId, message.Data)
                        }
                    };

                    _notifier.SendGameMessage(outgoingMessage);
                    break;
            }           
        }

    }
}