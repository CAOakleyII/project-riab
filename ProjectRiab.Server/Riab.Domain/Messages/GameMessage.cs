﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riab.Domain.Messages
{
    public class GameMessage : IGameMessage
    {
        public string RoomId { get; set; }
        public string MessageType { get; set; }
        public dynamic Data { get; set; }
    }
}