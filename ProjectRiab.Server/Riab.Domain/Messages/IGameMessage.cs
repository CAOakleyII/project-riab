﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riab.Domain.Messages
{
    public interface IGameMessage
    {
        string MessageType { get; set; }
        dynamic Data { get; set; }
    }
}