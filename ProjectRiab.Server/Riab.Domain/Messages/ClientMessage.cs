﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riab.Domain.Messages
{
    public class ClientMessage
    {
        public string ConnectionId { get; set; }

        public string MessageType { get; set; }

        public dynamic Data { get; set; }
    }
}