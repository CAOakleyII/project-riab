﻿using Riab.Domain.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riab.Domain.Services
{
    public interface INotifier
    {
        /// <summary>
        /// Sends the message to all users in the room.
        /// </summary>
        /// <param name="package"></param>
        void SendGameMessage(GameMessage message);

        /// <summary>
        /// Sends a message to the specified player id.
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="package"></param>
        void SendGameMessageToPlayerId(string playerId, GameMessage message);
    }
}