﻿var options = {
    domain: "localhost:9999"
};

/*
if (typeof jQuery == 'undefined') {

    // load jquery
    var jqueryScript = document.createElement('script');
    jqueryScript.setAttribute('src', 'http://example.com/site.js');
    document.head.appendChild(jqueryScript);

}*/

$(function () {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'scripts/jquery.signalR-2.2.0.min.js';
    $('head').append(script);

    script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = options.domain + '/signalr/hubs';
    $('head').append(script);
});

$(function () {

    $.connection.hub.url = "signalr";
    // Reference the auto-generated proxy for the hub.  
    var chat = $.connection.chatHub;
    // Create a function that the hub can call back to display messages.
    chat.client.addNewMessageToPage = function (name, message) {
        // Add the message to the page. 
        $('#discussion').append('<li><strong>' + htmlEncode(name)
            + '</strong>: ' + htmlEncode(message) + '</li>');
    };
    // Get the user name and store it to prepend to messages.
    $('#displayname').val(prompt('Enter your name:', ''));
    // Set initial focus to message input box.  
    $('#message').focus();
    // Start the connection.
    $.connection.hub.start().done(function () {
        $('#sendmessage').click(function () {
            // Call the Send method on the hub. 
            chat.server.send($('#displayname').val(), $('#message').val());
            // Clear text box and reset focus for next comment. 
            $('#message').val('').focus();
        });
    });
});