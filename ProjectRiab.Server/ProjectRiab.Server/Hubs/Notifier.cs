﻿using Microsoft.AspNet.SignalR;
using Riab.Domain.Messages;
using Riab.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectRiab.Web.Hubs
{
    public class Notifier : INotifier
    {
        /// <summary>
        /// Sends the message to all users in the room.
        /// </summary>
        /// <param name="message"></param>
        public void SendGameMessage(GameMessage message)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<ServerHub>();
            context.Clients.Group(message.RoomId).gameMessage(message);
        }

        /// <summary>
        /// Sends a message to the specified player id.
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="message"></param>
        public void SendGameMessageToPlayerId(string playerId, GameMessage message)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<ServerHub>();
            context.Clients.Client(playerId).gameMessage(message);
        }
    }
}