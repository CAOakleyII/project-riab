﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using Riab.Domain.Messages;
using Riab.Domain.Models;
using Riab.Game;
using ProjectRiab.Web.Repositories;
using Riab.Domain.Entities;

namespace ProjectRiab.Web.Hubs
{
    public class ServerHub : Hub
    {
        private UserRepository _userRepository = RepositoryManager.UserRepository;
        private List<PlayRoom> _roomRepository = RepositoryManager.RoomRepository;

        public override Task OnConnected()
        {
            // pass off the logic so we can use returns to break if errors
            HandleOnConnected(Context.ConnectionId);     
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            // pass off the logic so we can use returns to break if errors
            HandleOnDisconnected(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        /// <summary>
        /// An incoming message from a client. Sets user information and passes to the game manager.
        /// </summary>
        /// <param name="message">The client message.</param>
        public void ClientMessage(ClientMessage message)
        {
            message.ConnectionId = Context.ConnectionId;

            var user = _userRepository.GetUserById(Context.ConnectionId);

            if (user == null)
            {
                // no user? error
                return;
            }

            var room = GetUserRoom(user);

            if (room == null)
            {
                return;
            }

            room.UserMessage(message);
        }

        private void HandleOnConnected(string connectionId)
        {
            var user = new User
            {
                Id = Context.ConnectionId
            };

            _userRepository.AddUser(user);
            
            // 1. find what room the user is in
            // 2. if in no room then player needs to join one

            // set up a default test room and put all connects in it for now
            var room = this.AddUserToRoom(user, "1");

            // probably don't want to just connect the user want to check if its
            // full and if the room is private or something, but for now this is fine.

            // ideas: connect like this but then have the room send a message back that its full to the user
            //          and let the user figure out what to do
            room.UserConnected(user);
        }

        private void HandleOnDisconnected(string connectionId)
        {
            var user = _userRepository.GetUserById(Context.ConnectionId);

            if (user == null)
            {
                // no user? error
                return;
            }

            var room = GetUserRoom(user);

            if (room == null)
            {
                return;
            }

            room.UserDisconnected(user);
        }

        private PlayRoom GetUserRoom(IUser user)
        {
            var connectId = Context.ConnectionId;
            var room = _roomRepository.Where(r => r.Users.Contains(user)).FirstOrDefault(); // maybe add a check to make sure they aren't in 2 rooms
            return room;
        }

        /// <summary>
        /// Adds a user the specified room id and adds them to the appropriate group. If the room doesn't exist it is created.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roomId"></param>
        /// <returns>The room the user was added to.</returns>
        private PlayRoom AddUserToRoom(IUser user, string roomId)
        {
            var room = _roomRepository.FirstOrDefault(r => r.Id == roomId);

            if (room == null)
            {
                var newRoom = new PlayRoom(roomId, new Notifier());
                _roomRepository.Add(newRoom);
                Task t1 = Groups.Add(user.Id, newRoom.Id); // not sure why it returns task, look into this more.
                //t1.Wait(); // example online had this wait. look into why.

                room = newRoom;
            }

            return room;
        }
    }
}