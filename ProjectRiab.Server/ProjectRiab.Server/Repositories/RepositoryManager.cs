﻿using Riab.Domain.Entities;
using Riab.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectRiab.Web.Repositories
{
    public static class RepositoryManager
    {
        static RepositoryManager()
        {
            UserRepository = new UserRepository();
            RoomRepository = new List<PlayRoom>();
        }

        public static UserRepository UserRepository { get; private set; }

        public static List<PlayRoom> RoomRepository { get; private set; }
    }
}