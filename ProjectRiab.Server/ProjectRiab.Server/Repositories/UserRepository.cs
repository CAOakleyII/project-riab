﻿using Riab.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectRiab.Web.Repositories
{
    public class UserRepository
    {
        private List<IUser> _allUsers;

        public UserRepository()
        {
            _allUsers = new List<IUser>();
        }

        public void AddUser(IUser user)
        {
            _allUsers.Add(user);
        }

        public IUser GetUserById(string id)
        {
            return _allUsers.FirstOrDefault(u => u.Id == id);
        }

        public bool RemoveUserById(string id)
        {
            var user = GetUserById(id);

            if (user == null)
            {
                return false;
            }

            _allUsers.Remove(user);

            return true;
        }
    }
}