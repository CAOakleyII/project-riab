Physics = function(stage, level) {
  this.gravity = 5;
  this.stage = stage;
  this.level = level;
};

Physics.isColliding = function(rectA, rectB){
  
  // adjust rectangles based on orientation
  var rect1 = new PIXI.Rectangle(rectA.x, rectA.y, rectA.width, rectA.height);
  if(rect1.width < 0) {
    rect1.x += rect1.width;
    rect1.width = -1 * rect1.width;
  }
  if(rect1.height < 0) {
    rect1.y += rect1.height;
    rect1.height = -1 * rect1.height;
  }
  
  // adjust rectangles based on orientation
  var rect2 = new PIXI.Rectangle(rectB.x, rectB.y, rectB.width, rectB.height);
  if(rect2.width < 0){
    rect2.x += rect2.width;
    rect2.x = -1 * rect2.width;
  }
  if(rect2.height < 0){
    rect2.y += rect2.height;
    rect2.y = -1 * rect2.height;
  }
  
  if (rect1.x < rect2.x + rect2.width &&
   rect1.x + rect1.width > rect2.x &&
   rect1.y < rect2.y + rect2.height &&
   rect1.height + rect1.y > rect2.y) {
    // collision detected!
    return true;
  }
  return false;
};

Physics.prototype.applyGravity = function(sprite){
  if (sprite.isGravitational){
    
    // 5 is just a little padding from the actual border and sprite , can be changed to whaterver.
    var bottomOfhitBox = new PIXI.Point(sprite.hitBox.x, sprite.hitBox.y + sprite.hitBox.height + 5 + this.gravity);
    if (!this.isPointOnBottomOfLevel(bottomOfhitBox))
    {
      sprite.position = new PIXI.Point(sprite.x, sprite.y + this.gravity);
    }
  }
};

Physics.isPointOutsideFrame = function(point) {
  if (point.x >= 1024 || point.x <= 0)
    return true;
  if (point.y >= 768 || point.y <= 0)
    return true;
};

Physics.prototype.isPointOnBottomOfLevel = function(point) { 
  if (point.y >= this.level.height) {
    return true;
  }
};

Physics.isPointOnBottomOfLevel = function(point) {
  if (point.y >= 738) { 
  return true;
  }
};