InputManager = function(player){
  this.player = player;
};

InputManager.inputType = {
  Up : 0,
  Right : 1,
  Down : 2,
  Left : 3,
  Jump: 4,
  Sprint : 5,
  AutoAttack : 6,
  Skill1 : 7,
  Skill2 : 8,
  Skill3 : 9
};

InputManager.getInputType = function(key)
{
  switch(key)
  {
    case 39:
      return InputManager.inputType.Right;
    case 37:
      return InputManager.inputType.Left;
    case 38:
      return InputManager.inputType.Up;
    case 40:
      return InputManager.inputType.Down;
    case 32:
      return InputManager.inputType.Jump;
    case 16:
      return InputManager.inputType.Sprint;
    case 70:
      return InputManager.inputType.AutoAttack;
    case 65:
      return InputManager.inputType.Skill1;
    case 83:
      return InputManager.inputType.Skill2;
    case 68:
      return InputManager.inputType.Skill3;
  }
};



InputManager.prototype.readInput = function(){
      if (!this.player.character.isAttacking) {
      
      // Right Movement
      if (InputManager.inputType.Right in keysDown) {
        if(this.player.character.currentSequence != "walk" || !this.player.character.playing){
          this.player.character.frameRate = 7;
          this.player.character.gotoAndPlay("walk");
        }
        
        this.player.character.scale.x = -1;
        this.player.character.orientation = Player.orientation.Right;
        this.player.character.addImpulse(this.player.character.orientation * this.player.character.speed, 0);
        this.player.character.isWalking = true;
      }
      
      // Left Movement
      if (InputManager.inputType.Left in keysDown) {
        if(this.player.character.currentSequence != "walk" || !this.player.character.playing){
          this.player.character.frameRate = 7;
          this.player.character.gotoAndPlay("walk");
        }
          
        this.player.character.scale.x = 1;
        this.player.character.orientation = Player.orientation.Left;
        this.player.character.addImpulse(this.player.character.orientation * this.player.character.speed, 0);
        this.player.character.isWalking = true;
      }
      
      if (!(InputManager.inputType.Left in keysDown) && !(InputManager.inputType.Right in keysDown))
      {
        if(this.player.character.isWalking){
          this.player.character.isWalking = false;
          this.player.character.frameRate = 2;
          this.player.character.gotoAndPlay("idle");
        }
      }
      
      if (InputManager.inputType.Sprint in keysDown) {
        this.player.character.frameRate = 10;
        if (this.player.character.speed < this.player.character.sprint)
          this.player.character.speed += 0.2;
      } else { 
        if (this.player.character.speed > this.player.character.baseSpeed)
          this.player.character.speed  -= 0.2;
      }
    }

    // currently only allows the this.player to jump once they hit the floor.
    // the last conditional needs to be changed so they can jump once they are considered
    // "standing on an object"
    
    if (InputManager.inputType.Jump in keysDown && !this.player.character.isJumping & this.player.character.isStandingOnObject()) { 
      this.player.character.state.jumping.duration = this.player.character.state.jumping.baseDuration;
      this.player.character.isJumping = true;
    }
    
    if(this.player.character.isJumping){
      
      // use hard coded value
      var jumpPeak = this.player.character.state.jumping.baseDuration / 2
      if(!this.player.character.isAttacking)
      {
        this.player.character.gotoAndPlay("jump");
      }
      
      if(this.player.character.state.jumping.duration > jumpPeak)
      {
        this.player.character.addImpulse(0, -10);
        this.player.character.state.jumping.duration -= 1;
      }
      else if (this.player.character.state.jumping.duration <= jumpPeak && this.player.character.state.jumping.duration > 0)
      {
        this.player.character.state.jumping.duration -= 1;
      }
      else if(this.player.character.state.jumping.duration <= 0)
      {
      
        this.player.character.gotoAndPlay('idle');
        this.player.character.isJumping = false;
      }
      
    }
  
    
    // Auto Attack
    if (InputManager.inputType.AutoAttack in keysDown && !this.player.character.isAttacking)
    {
      this.player.character.frameRate = 7;
      this.player.character.isAttacking = true;
      this.player.character.gotoAndPlay("autoAttack");
      
            var orientationOffset = this.player.character.orientation == Player.orientation.Right ? this.player.character.hitBox.width : 0;
      
      var skillRect = new PIXI.Rectangle(this.player.character.hitBox.x + orientationOffset, 
                                        this.player.character.y - (this.player.character.height / 2), 
                                        this.player.character.orientation * 70,this.player.character.height);
                                        
      this.player.character.debugGraphic.lineStyle(1, 0xFFFFFF);
      this.player.character.debugGraphic.drawRect(skillRect.x, skillRect.y, skillRect.width, skillRect.height);
      
      for (var x = 0; x < this.enemies.length; x++){
        var enemy = this.enemies[x];
        if(Physics.isColliding(skillRect, enemy.hitBox))
        {
          enemy.currentHealth -= 10;
        }
      }
    }
    
    
    // Skill 1
    if (InputManager.inputType.Skill1 in keysDown && !this.player.character.isAttacking){
      this.player.character.frameRate = 7;
      this.player.character.isAttacking = true;
      this.player.character.gotoAndPlay("skill1");
      
            var orientationOffset = this.player.character.orientation == Player.orientation.Right ? this.player.character.hitBox.width : 0;
      
      var skillRect = new PIXI.Rectangle(this.player.character.hitBox.x + orientationOffset, 
                                        this.player.character.y - (this.player.character.height / 2), 
                                        this.player.character.orientation * 70,this.player.character.height);
      
      this.player.character.debugGraphic.lineStyle(1, 0xFFFFFF);
      this.player.character.debugGraphic.drawRect(skillRect.x, skillRect.y, skillRect.width, skillRect.height);
      
      for (var x = 0; x < this.enemies.length; x++){
        var enemy = this.enemies[x];
        if(Physics.isColliding(skillRect, enemy.hitBox))
        {
          enemy.currentHealth -= 5;
        }
      }
    }
    
    // Skill 2
    if (InputManager.inputType.Skill2 in keysDown && !this.player.character.isAttacking){
      this.player.character.frameRate = 7;
      this.player.character.isAttacking = true;
      this.player.character.gotoAndPlay("skill2");
      
      var orientationOffset = this.player.character.orientation == Player.orientation.Right ? this.player.character.hitBox.width : 0;
      
      var skillRect = new PIXI.Rectangle(this.player.character.hitBox.x + orientationOffset, 
                                        this.player.character.y - (this.player.character.height / 2), 
                                        this.player.character.orientation * 95,this.player.character.height);
                                        
      this.player.character.debugGraphic.lineStyle(1, 0xFFFFFF);
      this.player.character.debugGraphic.drawRect(skillRect.x, skillRect.y, skillRect.width, skillRect.height);
      
      for (var x = 0; x < this.enemies.length; x++){
        var enemy = this.enemies[x];
        if(Physics.isColliding(skillRect, enemy.hitBox))
        {
          enemy.currentHealth -= 15;
        }
      }
    }
    
    // Skill 3
    if (InputManager.inputType.Skill3 in keysDown && !this.player.character.isAttacking){
      this.player.character.frameRate = 7;
      this.player.character.isAttacking = true;
      this.player.character.gotoAndPlay("skill3");
      
      var orientationOffset = this.player.character.orientation == Player.orientation.Right ? this.player.character.hitBox.width : 0;
      
      var skillRect = new PIXI.Rectangle(this.player.character.hitBox.x + orientationOffset, 
                                        this.player.character.y - (this.player.character.height / 2), 
                                        this.player.character.orientation * 140,this.player.character.height);
      
      this.player.character.debugGraphic.lineStyle(1, 0xFFFFFF);
      this.player.character.debugGraphic.drawRect(skillRect.x, skillRect.y, skillRect.width, skillRect.height);
      
      for (var x = 0; x < this.enemies.length; x++){
        var enemy = this.enemies[x];
        if(Physics.isColliding(skillRect, enemy.hitBox))
        {
          enemy.currentHealth -= 30;
        }
      }
    }
    
    this.player.character.advanceTime(1/60);
    
                                    
  // check if feet are touching level. (if so make character stand)
  // need to change once other world objects are in game to looop through 
  // objects and see if it's standing on anyhting
  if (!this.player.character.playing && !this.player.character.isWalking && this.player.character.isStandingOnObject()){
    
    if (this.player.character.state.idle.duration > 0){
      this.player.character.state.idle.duration -= 1;
    }
    else { 
      this.player.character.frameRate = 2;
      this.player.character.gotoAndPlay("idle");
      this.player.character.state.idle.duration = 15;
    }
  }
};


var keysDown = [];

$(document).keydown(function(e){
	
	// space and arrow keys
  // prevent them from scrolling the page
	if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
	  e.preventDefault();
  }
  
  var input = InputManager.getInputType(e.keyCode);
  keysDown[input] = true;
  
});

$(document).keyup(function(e) {
  var input = InputManager.getInputType(e.keyCode);
  delete keysDown[input];
});
