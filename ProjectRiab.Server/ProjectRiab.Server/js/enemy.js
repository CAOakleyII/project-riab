Enemy = function(maxHealth, sequences) {
  
  // ball base constructor 
  PIXI.AnimatedSprite.call(this, sequences);
  
  // set settings that needed to be set after loading base object
  this.takesPlayerDamage = true;
  this.maxHealth = maxHealth;
  this.currentHealth = maxHealth;
  this.healthBar = new PIXI.Graphics();
  this.healthBar.beginFill(0xFF0000);
  this.healthBar.lineStyle(1, 0xFFFFFF);
  this.healthBar
    .drawRect(10, 
              10,
              (this.currentHealth / this.maxHealth) * 1000,
              10);
  this.isPhysicalObject = true;
  this.frames = null;
  
  this.hitBox = new PIXI.Rectangle(this.position.x - (this.width / 2), this.position.y - (this.height / 2) , this.width, this.height);
  
};

Enemy.constructor = Enemy;
Enemy.prototype = Object.create(PIXI.AnimatedSprite.prototype);


Enemy.prototype.isOutsideFrame = function(point) {
  if (point.x >= 800 || point.x <= 0)
    return true;
  if (point.y >= 400 || point.y <= 0)
    return true;
};

Enemy.prototype.updateHealthBar = function() {
  this.healthBar.width = (this.currentHealth / this.maxHealth) * 1000;
};