Character = function(sequences) {
  
  // ball base constructor 
  PIXI.AnimatedSprite.call(this, sequences);
  
  // set settings that needed to be set after loading base object
  
  // basic stats 
  this.baseSpeed= 2;
  this.speed = 2;
  this.sprint = 5;
  this.baseJump = 0;
  this.currentJump = 0;
  this.maxJump = 8;
  this.isJumping = false;
  this.orientation = Player.orientation.Right; 
  
  // states
  this.state = 
  { 
    "idle" : new State("idle", 0), 
    "jumping" : new State("jumping", 30),
    "walking" : new State("walking", 0),
    "autoAttacking" : new State("autoAttacking", 0),
    "skill1" : new State("skill1", 25),
    "skill2" : new State("skill2", 15),
    "skill3" : new State("skill3", 25),
    "attacking" : new State("attacking", 0)
  };
  
  // health stats
  this.maxHealth = 150;
  this.currentHealth = 150;
  this.healthBar = new PIXI.Graphics();
  this.healthBar.beginFill(0xFF0000);
  this.healthBar.lineStyle(1, 0xFFFFFF);
  this.healthBar
    .drawRect(this.position.x, 
              this.position.y + 15,
              (this.currentHealth / this.maxHealth) * 100,
              5);
  this.isPhysicalObject = true;
  this.hitBox = new PIXI.Rectangle(this.position.x - (this.width / 2), this.position.y - (this.height / 2) , this.width, this.height);
  
  // debug graphic
  this.debugGraphic = new PIXI.Graphics();
};

Character.constructor = Character;
Character.prototype = Object.create(PIXI.AnimatedSprite.prototype);

Character.prototype.isStandingOnObject = function(){
  return Physics.isPointOnBottomOfLevel(new PIXI.Point(this.hitBox.x, this.hitBox.y + this.hitBox.height + 9));
};

Character.prototype.updateHealthBar = function() {
  
    this.healthBar.position.x = this.hitBox.x - (10);
    this.healthBar.position.y = this.hitBox.y - (35);
};

Character.prototype.isOutsideFrame = function(point) {
  if (point.x >= 800 || point.x <= 0)
    return true;
  if (point.y >= 400 || point.y <= 0)
    return true;
};