State = function(name, duration)
{
  this.name = name;
  this.baseDuration = duration;
  this.duration = duration;
  this.isActive = false;
};

State.prototype.update = function(){
  if (this.active) {
    this.duration -= 1;
  }
  if (this.duration === 0){
    this.isActive = false;
  }
};