window.onload = function() {

  var renderer = PIXI.autoDetectRenderer(1024, 768);
  
  $('body').append(renderer.view);
  
  // create the root of the scene graph
  var stage = new PIXI.Container();
  var level = new PIXI.Rectangle(0,0,1024, 738);
  
  PIXI.loader
      .add('assets/basics/warrior_walk_1.json')
      .add('assets/basics/warrior_jump.json')
      .add('assets/basics/warrior_stand.json')
      .add('assets/basics/warrior_swing_O1.json')
      .add('assets/basics/warrior_swing_O2.json')
      .add('assets/basics/warrior_swing_O3.json')
      .add('assets/basics/warrior_swing_OF.json')
      .add('assets/basics/wyvern.json')
      .load(onAssetsLoaded);
  
  var Acceleration = 1.3;
  var player;
  var playersInfo;
  var frameBoxes;
  var enemies = [];
  var enemy;
  var physics = new Physics(stage, level);
  
  function loadPlayer()
  {
    player = new Player("warrior");
  
    stage.addChild(player.character);
    stage.addChild(player.character.healthBar);
  }


  function loadEnemies() {
    enemy = new Wyvern(1000);
    enemy.position.x = 700;
    enemy.position.y = 300;
    enemies.push(enemy);
    
    
    frameBoxes.lineStyle(1, 0xFFFFFF);
    
    frameBoxes.drawRect(enemy.hitBox.x, enemy.hitBox.y, enemy.hitBox.width, enemy.hitBox.height);
    
    stage.addChild(enemy);
    stage.addChild(enemy.healthBar);
  }
  
  function loadDebug(){
    var style = {
      font : '10px Arial',
      fill : '#FFFFFF',
      stroke : '#FFFFFF'  
    };
    
    playersInfo = new PIXI.Text("Width: " + player.character.width + "\n" +
                                    "Height: " + player.character.height + "\n" +
                                    "X: " + player.character.position.x + "\n" +
                                    "Y: " + player.character.position.y ,style);
    
    frameBoxes = new PIXI.Graphics();
    stage.addChild(player.character.debugGraphic);
    playersInfo.x = 15;
    playersInfo.y = 35;
    stage.addChild(playersInfo);
    stage.addChild(frameBoxes);
  }
  
  function onAssetsLoaded()
  {
      loadPlayer();
      loadDebug();
      loadEnemies();
      player.input.enemies = enemies;
      animate();
  }

  // *** GAME LOOP ***
  function animate() {
    
    player.input.readInput();
    
    for (var i = 0; i < stage.children.length; i++)
    {
      physics.applyGravity(stage.children[i]);
    }
    player.character.updateHealthBar();
    player.character.updateHitBox();
    enemy.updateHealthBar();
    enemy.updateHitBox();
    
    // debug lines
    playersInfo.text = "Width: " + player.character.width + "\n" +
                                    "Height: " + player.character.height + "\n" +
                                    "X: " + player.character.position.x + "\n" +
                                    "Y: " + player.character.position.y + "\n" +
                                    "enemy health :"+ enemy.currentHealth + "\n" +
                                    "enemy x :" + enemy.position.x + "\n" +
                                    "enemy y " + enemy.position.y + "\n" +
                                    "enemy width" + enemy.height;
                                    
    frameBoxes.clear();
    frameBoxes.lineStyle(1, 0xFFFFFF);
    
    frameBoxes.drawRect(enemy.hitBox.x, enemy.hitBox.y, enemy.hitBox.width, enemy.hitBox.height);
    frameBoxes.drawRect(player.character.hitBox.x, player.character.hitBox.y, player.character.hitBox.width, player.character.hitBox.height);
    // end debug lines
    
    // render the stage container
    renderer.render(stage);

    requestAnimationFrame(animate);
  }
};


$(function () {
    $.connection.hub.logging = true;
    var serverHub = $.connection.serverHub;
    serverHub.client.gameMessage = function (gameMessage) {
        HandleServerMessage(gameMessage);
    };

    $.connection.hub.start().done(function () {
        SignalRInitialized(serverHub);
    });
});
var gameConnection;
function SignalRInitialized(hub) {
    gameConnection = hub;
    // send a message to the server when we are connected
    var message = {
        MessageType: "Message",
        Data: {
            Message: "i connected"
        }
    }
    gameConnection.server.clientMessage(message);
}

function HandleServerMessage(serverMessage) {
    switch (serverMessage.MessageType) {
        case "Message":
            console.log(serverMessage.Data.Message);
            break;
        case "GameState":
            console.log("Gamestate: " + serverMessage.Data.Time);
            break;
        default:
            console.log("unknown message type recieved from server.");
            break;
    }
}
