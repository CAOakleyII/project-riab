Warrior = function(){
  var sequences = loadWarriorFrames();
  Character.call(this, sequences);
};

var loadWarriorFrames = function(){
  
    var walkFrames = [];

    for (var i = 0; i < 4; i++) {
      walkFrames.push(PIXI.Texture.fromFrame('warriorWalk' + i + ".png"));
    }
    
    var jumpFrames = [];
    
    for (var x =0; x < 1; x++) {
      jumpFrames.push(PIXI.Texture.fromFrame("warriorJump" + x + ".png"));
    }
    
    var standFrames = [];
    
    for (var y = 0; y < 3; y++) {
      standFrames.push(PIXI.Texture.fromFrame("warriorStand" + y + ".png"));
    }
    
    var swingO1Frames = [];
    
    for (var z = 0; z < 3; z++){
      swingO1Frames.push(PIXI.Texture.fromFrame("warriorSwingO1_" + z + ".png"));
    }
    
    var swingO2Frames = [];
    
    for (var a = 0; a < 3; a++){
      swingO2Frames.push(PIXI.Texture.fromFrame("warriorSwingO2_" + a + ".png"));
    }
    
    var swingO3Frames = [];
    
    for (var b = 0; b <3; b++){
      swingO3Frames.push(PIXI.Texture.fromFrame("warriorSwingO3_" + b + ".png"));
    }
    
    var swingOFFrames = [];
    
    for (var c = 0; c < 4; c++) {
      swingOFFrames.push(PIXI.Texture.fromFrame("warriorSwingOF_" + c + ".png"));
    }
    return {
      "idle": standFrames,
      "walk" : walkFrames,
      "jump" : jumpFrames,
      "autoAttack" : swingO1Frames,
      "skill1"  : swingO2Frames,
      "skill2" : swingO3Frames,
      "skill3" : swingOFFrames
    };
  
};

Warrior.constructor = Warrior;
Warrior.prototype = Object.create(Character.prototype);