var MAX_HEALTH = 10000;
Wyvern = function() {
  var sequences = loadFrames();
  Enemy.call(this, MAX_HEALTH, sequences);
  
};


var loadFrames = function(){
  var idleFrames = [];
  
  for (var i = 0; i < 1; i++) {
    idleFrames.push(PIXI.Texture.fromFrame('wyvern.png'));
  }
  
  var sequences = {
    "idle" : idleFrames
  };
  
  return sequences;
};

Wyvern.constructor = Wyvern;
Wyvern.prototype = Object.create(Enemy.prototype);