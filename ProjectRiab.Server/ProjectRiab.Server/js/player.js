Player = function(className){
    
    
    
    this.character = null;
    this.sequences = null;
    this.input = new InputManager(this);
    var sequences = loadFrames();
    
    switch(className)
    {
      case "warrior":
      this.character = new Warrior(sequences);
      break;
    }
    
    this.character.frameRate = 2;
    this.character.position.x = 300;
    this.character.position.y = 300;
    this.character.onComplete = this.completedSequences;
    this.character.play();
};


Player.orientation = {
  Left : -1,
  Right : 1
  
};



Player.prototype.completedSequences = function(character, completed)
{
  // oddly enough this in this context is referencing the character, and not the player.
  // I have no idea why, but i'm just going to roll with it.
  // console.log(this);
  this.isAttacking = false;
  if (completed == "autoAttack") {
    character.gotoAndPlay("idle");
    this.debugGraphic.clear();
  }
  if (completed == "skill1") {
    character.gotoAndPlay("idle");
    this.debugGraphic.clear();
  }
  
  if (completed == "skill2") {
    character.gotoAndPlay("idle");
    this.debugGraphic.clear();
  }
  
  if (completed == "skill3") {
    character.gotoAndPlay("idle");
    this.debugGraphic.clear();
  }
};